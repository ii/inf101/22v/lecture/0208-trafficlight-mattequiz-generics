package inf101v22.generics;

public class Pair <A, B> {
    
    public final A first;
    public final B second;

    public Pair(A first, B second) {
        this.first = first;
        this.second = second;
    }

}
