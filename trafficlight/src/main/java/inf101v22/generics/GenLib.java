package inf101v22.generics;

import java.util.List;
import java.util.Objects;

public class GenLib {

    public static <S, T> boolean areSwapped(Pair<S, T> p1, Pair<T, S> p2) {

        return Objects.equals(p1.first, p2.second)
            && Objects.equals(p1.second, p2.first);
    }


    public static <K extends Comparable<K>> K findLargest(List<K> items) {
        K mostValuableSoFar = items.get(0);
        for (K item : items) {
            if (item.compareTo(mostValuableSoFar) > 0) {
                mostValuableSoFar = item;
            }
        }
        return mostValuableSoFar;
    }
    
}
