package inf101v22.eksempel;

import java.util.Date;

public class Timer {

    /**
     * Time how long it takes to run the method
     * @param method the method to run
     * @return how long it took to run the method
     */
    public static long timeIt(Runnable method) {
        Date timeBefore = new Date();
        method.run();
        Date timeAfter = new Date();
        return timeAfter.getTime() - timeBefore.getTime();
    }
}