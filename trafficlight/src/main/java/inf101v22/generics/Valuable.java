package inf101v22.generics;

public interface Valuable {
    double getValue();
}
