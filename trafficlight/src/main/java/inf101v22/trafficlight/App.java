package inf101v22.trafficlight;

import inf101v22.trafficlight.TrafficLight.AlreadyStartedException;

public class App {
    
    public static void main(String[] args) throws AlreadyStartedException {
        TrafficLight trafficLight = new TrafficLight();

        // // Setting light to green
        // trafficLight.state = TrafficLight.State.GO; 

        // // Changed my mind
        // trafficLight.state = TrafficLight.State.STOP;

        trafficLight.turnOn();
        System.out.println(trafficLight);
        
        trafficLight.goToNextState();
        System.out.println(trafficLight);

        trafficLight.goToRed();
        System.out.println(trafficLight);

        for (int i = 0; i < 15; i++) {
            trafficLight.goToNextState();
            System.out.println(trafficLight);
        }
    }

    
}
