package inf101v22.trafficlight;

public interface ITrafficLight {
    
    /**
     * Change state so the light becomes green
     */
    void goToGreen();

    /**
     * Change state so the light becomes red
     */
    void goToRed();

    /**
     * 
     * @return true if green light is on, false otherwise
     */
    boolean getGreen();

    /**
     * 
     * @return true if yellow light is on, false otherwise
     */
    boolean getYellow();

    /**
     * 
     * @return true if red light is on, false otherwise
     */
    boolean getRed();

}
