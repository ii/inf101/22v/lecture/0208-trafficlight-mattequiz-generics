package inf101v22.generics;

public class Box <MyBoxedType> {

    private MyBoxedType thing;
    
    public void put(MyBoxedType thing) {
        this.thing = thing;
    }

    public MyBoxedType pick() {
        MyBoxedType returnValue = this.thing;
        this.thing = null;
        return returnValue;
    }
}
